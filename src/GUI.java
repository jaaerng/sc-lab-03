import java.awt.Font;
import java.awt.TextArea;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class GUI extends JFrame{

	private Tokenizer token = new Tokenizer();
	private TextArea out = new TextArea(20,40);
	private TextArea in = new TextArea(20,40);
	private JPanel mainPanel = new JPanel();
	private String[] list;

	public GUI() {
		mainPanel.add(in);
		mainPanel.add(out);
		add(mainPanel);
		
		inputPanel();
		outputPanel();
		
		setContentPane(mainPanel);
		setTitle("UnAerng");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}

	public void inputPanel() {
		String  str = JOptionPane.showInputDialog(this, "what's your word?");
		String nGram = JOptionPane.showInputDialog(this, "Number of n-gram?");
		int num = Integer.parseInt(nGram);
		list = token.generateNGram(str, num);
		Font font = new Font(Font.SERIF, Font.BOLD, 16);
		in.setFont(font);
		in.setText(str);
	}

	public void outputPanel() {
		String str = "";
		for (String s : list) {
			str += s+"\n";
		}
		Font font = new Font(Font.SERIF, Font.BOLD, 16);
		out.setFont(font);
		out.setText(str);
	}
}
