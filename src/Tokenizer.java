
public class Tokenizer {
	
	public String[] generateNGram(String str, int n) {
		str = removeNonAlphabet(str);
		String[] list = new String[str.length() - (n-1)];
		for (int i=0; i<str.length() - (n-1); ++i) {
			String string = str.substring(i, i+n);
			list[i] = string;
		}
		return list;
	}
	
	public String removeNonAlphabet(String str) {
		String nonAlpha = "";
		for (int i=0; i<str.length(); ++i) {
			if (!checkValidChar(str.charAt(i))) {
				nonAlpha += str.charAt(i);
			}
		}
		for (int i=0; i<nonAlpha.length(); ++i) {
			str = str.replace(((Character)nonAlpha.charAt(i)).toString(), "");
		}
		return str;
	}
	
	private boolean checkValidChar(char c) {
		if ((c > 'a'-1 && c < 'z'+1) || (c > 'A'-1 && c < 'Z'+1)) {
			return true;
		}
		return false;
	}
}
